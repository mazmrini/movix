# movix

Merge Rubix clips into one. Speeds up scramble part and merge clips. Adds music over clip.

## Requirements

- Python 3.7+
- ImageMagick binary

## Local setup
```bash
python3 -m venv venv
source venv/bin/activate # windows: .\venv\Scripts\activate
pip3 install -r requirements.txt

pip3 install -e .

# specify magick.exe path in moviepy/config_defaults.py
```

## Run

```bash
python3 movix -h

# example
python3 movix config.yaml out.mp4
```

## Linting

```bash
mypy moviex
```