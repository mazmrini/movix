import setuptools

setuptools.setup(
    name="movix",
    version="0.0.0",
    author="mazine",
    description="rubix scramble editor",
    packages=setuptools.find_packages(),
    python_requires='>=3.7'
)
