"""
usage: [--version] [--help] <config> <out>

Mandatory args:
  config    Path to the config file
  out       Output file path

options:
  -v, --version
  -h, --help

"""
from docopt import docopt
import yaml
from movix.lib.validator import Validator
from movix.lib.config import Config
from movix.lib.clip import make_movix


class Args:
    def __init__(self, config: str, out: str) -> None:
        with open(config, 'r') as stream:
            config_dict = yaml.safe_load(stream)

        err = []
        err += Validator.validate_out(out)
        err += Validator.validate_config(config_dict)
        if len(err) > 0:
            raise Exception(err)

        self.config = Config(config_dict)
        self.out = out


if __name__ == '__main__':
    docopt_args = docopt(__doc__, version="0.0.0", options_first=True)
    args = Args(docopt_args['<config>'], docopt_args['<out>'])

    movix = make_movix(args.config.clips(), args.config.audio())
    movix.write_videofile(args.out)

    print('Done :)')
