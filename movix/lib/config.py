from typing import Dict, List
from moviepy.editor import VideoFileClip
from movix.lib.clip import Clip


class Config:
    CLIPS = 'clips'
    AUDIO = 'audio'
    KEYS = [CLIPS, AUDIO]

    CLIP_PATH = 'path'
    CLIP_TITLE = 'title'
    CLIP_SOLVE_START = 'solve_start'
    CLIP_KEYS = [CLIP_PATH, CLIP_TITLE, CLIP_SOLVE_START]

    def __init__(self, config: Dict[str, any]) -> None:
        self.config = config

    def clips(self) -> List[Clip]:
        return [self.__to_clip(clip_config) for clip_config in self.config[Config.CLIPS]]

    def audio(self) -> str:
        return self.config[Config.AUDIO]

    def __to_clip(self, clip_config) -> Clip:
        file_clip = VideoFileClip(clip_config[Config.CLIP_PATH])

        return Clip(file_clip, clip_config[Config.CLIP_TITLE], clip_config[Config.CLIP_SOLVE_START])
