from typing import List
from moviepy.editor import (
    VideoFileClip, CompositeVideoClip, VideoClip, TextClip, vfx, concatenate_videoclips, AudioFileClip
)


class Clip:
    SCRAMBLE_SPEED_FACTOR = 16

    def __init__(self, file_clip: VideoFileClip, title: str, solve_start: int) -> None:
        self.file_clip = file_clip
        self.title = title
        self.solve_start = solve_start

    def render(self) -> VideoClip:
        scramble_clip = (
            self.file_clip
                .subclip(0, self.solve_start)
                .fx(vfx.speedx, Clip.SCRAMBLE_SPEED_FACTOR)
                .fx(vfx.colorx, 0.4)
                .without_audio()
        )
        title_clip = (
            TextClip(self.title, fontsize=96, color='black', bg_color='white', size=(scramble_clip.size[0], 150))
            .set_duration(scramble_clip.duration)
            .margin(top=int(scramble_clip.size[1] * 0.25), opacity=0)
        )
        scramble_clip = CompositeVideoClip([scramble_clip, title_clip])
        solve_clip = (
            self.file_clip
                .subclip(t_start=self.solve_start)
                .without_audio()
        )

        return concatenate_videoclips([scramble_clip, solve_clip])


def make_movix(clips: List[Clip], audio_file: str) -> VideoClip:
    rendered = [clip.render() for clip in clips]
    movix = concatenate_videoclips(rendered)

    audio_clip = AudioFileClip(audio_file)
    audio_duration = min(movix.duration, audio_clip.duration)
    audio_clip = audio_clip.subclip(0, audio_duration)

    return movix.set_audio(audio_clip)
