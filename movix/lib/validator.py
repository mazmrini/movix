from typing import List, Dict
from pathlib import Path
from movix.lib.config import Config


class Validator:
    @staticmethod
    def validate_out(out: str) -> List[str]:
        out_path = Path(out)

        if not out_path.parent.is_dir():
            return [f"out [{out}] directory must exist"]

        if out_path.suffix != ".mp4":
            return [f"out [{out}] must be an mp4 file"]

        if out_path.is_file():
            return [f"out [{out}] currently exists and should not"]

        return []

    @staticmethod
    def validate_config(config: Dict[str, any]) -> List[str]:
        if not all(key in Config.KEYS for key in config.keys()):
            return [f"Config file must have all keys from {Config.KEYS}"]

        err = []
        for clip_config in config[Config.CLIPS]:
            err += Validator.__validate_clip_config(clip_config)

        err += Validator.__validate_audio(config[Config.AUDIO])

        return err

    @staticmethod
    def __validate_clip_config(clip_config: Dict[str, any]) -> List[str]:
        if not all(key in Config.CLIP_KEYS for key in clip_config.keys()):
            return [f"Single clip must have all keys from {Config.CLIP_KEYS}"]

        err = []
        clip_path = Path(clip_config[Config.CLIP_PATH])
        if not clip_path.is_file():
            err += [f"clip path [{clip_path}] must exist"]

        if not isinstance(clip_config[Config.CLIP_TITLE], str):
            err += [f"title of [{clip_path}] must be a string"]

        if not isinstance(clip_config[Config.CLIP_SOLVE_START], int):
            err += [f"solve_start time of [{clip_path}] must be an integer"]
        else:
            if clip_config[Config.CLIP_SOLVE_START] <= 0:
                err += ["solve_start time of [{clip_path}] must be greater than 0"]

        return err

    @staticmethod
    def __validate_audio(audio_file: str) -> List[str]:
        if not Path(audio_file).is_file():
            return [f"audio file [{audio_file}] must exist"]

        return []